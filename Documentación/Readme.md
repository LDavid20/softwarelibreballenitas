![coamdo tree](img/0.jpg "Archivo de Imagen").

Desarrollar habilidades en programación de aplicaciones comerciales con Software Libre,
mediante la creación de una aplicación Web y de Línea de Comandos, que se integre con
aplicaciones/librerías de terceros



# Instalación de Laravel

Para la realización de este apartado, nos dirigimos a la siguiente dirección página de `Laravel` la cual sería la siguiente: `https://laravel.com/`

En ella podremos encontrar toda la documentación necesaria para la instalación de Laravel, para eso nos vamos al apartado de documentación donde nos encontraremos los requerimientos que serían los siguientes

![coamdo tree](img/1.jpg "Archivo de Imagen")

Entre el requerimiento más importante nos encontramos con `php`, del cual lo obtendremos con la instalación a su vez de `Xampp`, para ello nos vamos al siguiente dirección:`https://www.apachefriends.org/es/index.html`, la instalación de Xampp se realiza rápidamente, le damos a descargar, luego ejecutamos el archivo `.exe`, donde le daremos siguiente a todo y con esto tendremos Xampp instalado.

Posteriormente si queremos  identificar la versión de nuestro php, vamos a tener que realizar el siguiente comando 

![coamdo tree](img/2.jpg "Archivo de Imagen")


A su vez, si continuamos leyendo la documentación nos solicitan la instalación de `compouser`.

![coamdo tree](img/3.jpg "Archivo de Imagen")

Para ello nos dirigiremos a la `PowerShell` de Windows y ejecutamos el siguiente comando `composer global require laravel/installer`, el cual nos instalara el compuoser y el Laravel

![coamdo tree](img/4.jpg "Archivo de Imagen")

Ahora bien, ejecutamos Xampp dejando de la siguiente forma.

![coamdo tree](img/5.jpg "Archivo de Imagen")

Ahora bien proseguimos con creacion de un proyecto en `Laravel`
Inicialmente nos cambiamos de ruta, que seria la siguiente:

![coamdo tree](img/6.jpg "Archivo de Imagen")

En esta ruta vamos a ejecutar el siguiente comando `composer create-project laravel/laravel="7.6.*" SoftwareLibreBallenitas`

![coamdo tree](img/7.jpg "Archivo de Imagen")


El comando anterior funciona para crear el proyecto de Laravel, por ende puede  tardar varios minutos, donde habrá que ser pacientes

Para iniciar el proyecto, hay que recordar que debemos de tener  Xampp corriendo y luego nos vamos a la siguiente ubicación:

![coamdo tree](img/8.jpg "Archivo de Imagen")

Seleccionada la carpeta del proyecto nos saltra lo siguiente, donde tendremos que darle a la carpeta `puplic`

![coamdo tree](img/9.jpg "Archivo de Imagen")

Con esto nos cargara el inicio de Laravel, ya con esto comprobamos que la instalacion se ejecuto perfectamente.

![coamdo tree](img/10.jpg "Archivo de Imagen")

# Creación del Login y Registrar

Inicialmente se realizara una estructura para cada pantalla que el usuario utilizara 

Para esto vamos a crear varios archivo como plantilla, login ...

![coamdo tree](img/11.jpg "Archivo de Imagen")

Luego detro de `plantilla.blade.php` crearemos la estructura necesaria, la cual seria por parte para un mayor orden y control. Luego llamos estas parte por medio del comando de `yield`.

![coamdo tree](img/12.jpg "Archivo de Imagen")

Este deria el contenido del archivo principal, con su caracteristicas curiosas.

![coamdo tree](img/13.jpg "Archivo de Imagen")

Esta deria la estructura del `Registro de usuario`

![coamdo tree](img/14.jpg "Archivo de Imagen")

Basandonos en el diseño anterior creariamos el login

![coamdo tree](img/15.jpg "Archivo de Imagen")

Asi mismo creariamos las demas parte como seria el `footer`

![coamdo tree](img/16.jpg "Archivo de Imagen")

La barra de navegacio

![coamdo tree](img/17.jpg "Archivo de Imagen")

En el caso de ruteo utilizareamos rutas, por si llegaramos a modificar el archivo no realizar muchos cambios

![coamdo tree](img/18.jpg "Archivo de Imagen")

Graficamente asi se veria el `Registrar`

![coamdo tree](img/19.jpg "Archivo de Imagen")

El `Login`

![coamdo tree](img/20.jpg "Archivo de Imagen")

La pagina `Principal`

![coamdo tree](img/21.jpg "Archivo de Imagen")

En la misma pagina principal creamos el equipo desarrollo del proyecto

![coamdo tree](img/22.jpg "Archivo de Imagen")



# Implementación de Auth de Laravel

Para esto primero instalaremos una librería, pero al instalarla daba errores, así lo que se tuvo que hacer fue lo siguiente 
![coamdo tree](img/23.jpg "Archivo de Imagen")
Se tuvieron que agregar las dos siguientes líneas de comandos:

![coamdo tree](img/24.jpg "Archivo de Imagen")

![coamdo tree](img/25.jpg "Archivo de Imagen")

Y luego procedimos a instalar la librería que nos daba error `composer require laravel/ui` y luego la siguiente línea de comando para que esta nos cree la carpeta de `auth`

![coamdo tree](img/26.jpg "Archivo de Imagen")

En la carpeta auth se pueden ver los siguientes archivos 

![coamdo tree](img/27.jpg "Archivo de Imagen")

Luego de eso vamos a migrar las tablas que tenemos a la base de datos con el siguiente comando `php artisan migrate`

![coamdo tree](img/28.jpg "Archivo de Imagen")

Y si nos vamos a la base de datos podremos ver las tablas

![coamdo tree](img/29.jpg "Archivo de Imagen")

El comando anterioente creado nos va a crear el archivo de `Registro`

![coamdo tree](img/30.jpg "Archivo de Imagen")

Asi mismo como el de `Login`

![coamdo tree](img/31.jpg "Archivo de Imagen")


# Implementación de RabbitMQ

Primero entraremos el siguiente link `https://customer.cloudamqp.com/login` en el cual nos piden registrarnos 

![coamdo tree](img/32.jpg "Archivo de Imagen")

Por lo menos en nuestro caso nos registraremos con `Google`

![coamdo tree](img/33.jpg "Archivo de Imagen")

Luego de eso aceptaremos los permisos de servicio y de política

![coamdo tree](img/34.jpg "Archivo de Imagen")

Luego vamos a crear una nueva instancia 

![coamdo tree](img/35.jpg "Archivo de Imagen")

Y nos mostrara los siguientes campos

![coamdo tree](img/36.jpg "Archivo de Imagen")

En el campo de `Name` le pusimos `ballenitaspiratas`

![coamdo tree](img/37.jpg "Archivo de Imagen")

luego le damos en el que dice `select Region`

![coamdo tree](img/38.jpg "Archivo de Imagen")

En `select Region` escogeremos la siguiente región 

![coamdo tree](img/39.jpg "Archivo de Imagen")

Le damos click en el siguiente cuadro 

![coamdo tree](img/40.jpg "Archivo de Imagen")

y a continuación le damos crear la instancia

![coamdo tree](img/41.jpg "Archivo de Imagen")

Y nos mostrara la instancia creada

![coamdo tree](img/42.jpg "Archivo de Imagen")

Dándole click a la instancia, vamos a necesitar los siguientes datos: 

![coamdo tree](img/43.jpg "Archivo de Imagen")

![coamdo tree](img/44.jpg "Archivo de Imagen")

Luego en una terminal del proyecto escribiremos la siguiente línea `composer require php-amqplib/php-amqplib`

En caso que le diera error como fue a nosotros, parte del problema es por no actualizar el composer, para ello primero ingresamos el comando de `composer install` y luego el `composer update`.

Realizado estos cambios volveremos a ejecutar el comando que nos dio error y veremos que esta ves funcionara perfectamente

Luego iremos a la pagina de `RabbitMQ`, para inicial el tutorial numero uno. Este se encuentra en la siguiente pagina `https://www.rabbitmq.com/tutorials/tutorial-one-php.html`

![coamdo tree](img/45.jpg "Archivo de Imagen")

Con la documentación del sitio, tendremos que crear archivo uno llamado `send` y otro `receive`

![coamdo tree](img/46.jpg "Archivo de Imagen")

El caso de `receive` este contendra la siguiente información, la misma del documento en linea.

![coamdo tree](img/47.jpg "Archivo de Imagen")

En el archivo de `send` este tendra lo siguiente.

![coamdo tree](img/48.jpg "Archivo de Imagen")

Para retomar un poco recordar que este bien la ubicacion del archivo de `autoload`, asi mismo que en la connection tenga toda la informacion necesaria para su funcionamiento, esta misma la encontraremos en el sitio que nos logueamos anteriormente

![coamdo tree](img/49.jpg "Archivo de Imagen")

Ya con esto podremos testear el archivo, el cual nos brindara una respuesta de `Hello World`

![coamdo tree](img/50.jpg "Archivo de Imagen")

Con esto estariamos verificando que todo esta bien. Y ya podriamos seguir con el segundo tutorial, que busca brindar la capacidad de enviar varios archivo a la ves y dejarlos en cola con su debida prioridad

De igual forma que el primero tutorial crearemos dos archivo uno llamado `new_task` y el otro sera `worker`. En este tendra la siguiente información

![coamdo tree](img/51.jpg "Archivo de Imagen")

Y en el `new_tak` este tendra lo siguiente

![coamdo tree](img/52.jpg "Archivo de Imagen")

Luego de desarrollado lo anterior pordremos realizar una prueba muy interesante, en la cual en dos consolas ejecutaremos el archivo de `worker.php` y luego en una tercera consola vamos enviando diversos mensajes.

![coamdo tree](img/53.jpg "Archivo de Imagen")

Lo genial es que el mismo proyecto, era administrado estos mensajes y colocando en cada `worker`. En el caso de el primero worker llegara los mensajes de la primera, tercera y quinta

![coamdo tree](img/54.jpg "Archivo de Imagen")

En el otro worker llegaran el segundo y cuarto mensaje, que lo hacer ver genial

![coamdo tree](img/55.jpg "Archivo de Imagen")

Con esto finalizariamos este segundo tutotial de `RabbitMQ`



# Módulo CLI

entraemos a esta pagina  ` https://youtube-dl.org/  ` y descragamos el .

![coamdo tree](img/56.jpg "Archivo de Imagen") .exe

Luego entramos a esta `https://github.com/norkunas/youtube-dl-php`
donde ejecutamos `composer require norkunas/youtube-dl-php:dev-master`
 y tambien vamos a necesitar lo que es el siguiente codigo para descargar

![coamdo tree](img/57.jpg "Archivo de Imagen") 

Ademas se crea una carpeta la cual se llamo  `descargas_videos` donde se guardaran los videos descargados

![coamdo tree](img/58.jpg "Archivo de Imagen") 

Y ademas cambiaremos esta parte a la cual colocara el video en la carpeta que anteriormente creamos

![coamdo tree](img/59.jpg "Archivo de Imagen") 
