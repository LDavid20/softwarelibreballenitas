<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('inicio');
});

/*
Route::get('registro', function () {
    return view('registro');
})->name('registro');


Route::get('login', function () {
    return view('loginv1');
})->name('login');

*/

Route::get('inicio', function () {
   return view('inicio');
})->name('inicio');

 
 Auth::routes();
  Route::get('/home', 'HomeController@index')->name('home');

/*
  Route::get('home', function () {
    $descarga= App\Descargas::all();
    return $descarga;
return view('home');*/

Route::get('home', 'DescargasController@show');
Route::post('home', 'DescargasController@create');