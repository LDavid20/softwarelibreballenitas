<?php

namespace App\Http\Controllers;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use App\Descargas;
use Illuminate\Http\Request;;

use Illuminate\Support\Facades\Auth;

//include("new_task.php");

class DescargasController extends Controller
{
    //
    public function show()
    {

        $descargas = Descargas::all();
        return view('home', ['descargas' => $descargas]);
    }

    public function create()
    {
        $descarga = new Descargas();

        $descarga->id_user = request('id_user');
        $descarga->link = request('link');
        $descarga->estado = request('estado');

    
        $descarga->save();
        $this-> descargata($descarga);

      //  $gtdescar = Descargas::latest('id_descarga')->first();

        //return redirect('home');
    //echo $gtdescar ->id_descarga;
    }


    public function Update($dato)
    {

        $descarga = Descargas::find($dato->id);
        //$descarga->id_user = request('id_user');
       // $descarga->link = request('link');
        $descarga->estado = $dato->estado;
        //echo   $descarga->estado;
       // $descarga->save();
    }

    public function descargata(string $descarga)
    {
        $connection = new AMQPStreamConnection('shrimp-01.rmq.cloudamqp.com', 5672, 'jmxjqgay', 'go2z323WNFIl_6t32NEiWyePy3i6rZhF', 'jmxjqgay');
        $channel = $connection->channel();

        $channel->queue_declare('task_queue', false, true, false, false);

        $msg = new AMQPMessage($descarga);
        $channel->basic_publish($msg, '', 'task_queue');
        $channel->close();
        $connection->close();
    }

    public function descargar_youtube(string $link)
    {
        $command = "youtube-dl.exe ". $link;
        $descriptorspec = array(
            0 => array("pipe", "r"),  // stdin is a pipe that the child will read from
            1 => array("pipe", "w"),  // stdout is a pipe that the child will write to
            2 => array("file", "/tmp/error-output.txt", "a") // stderr is a file to write to
        );

        $process = proc_open($command, $descriptorspec, $pipes);

        if (is_resource($process)) {
            // $pipes now looks like this:
            // 0 => writeable handle connected to child stdin
            // 1 => readable handle connected to child stdout
            // Any error output will be appended to /tmp/error-output.txt

            fwrite($pipes[0], '<?php print_r($_ENV); ?>');
            fclose($pipes[0]);

            echo stream_get_contents($pipes[1]);
            fclose($pipes[1]);

            // It is important that you close any pipes before calling
            // proc_close in order to avoid a deadlock
            $return_value = proc_close($process);

         "command returned $return_value\n";
        }
    }

}
