@extends('layout')

@section('registro')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-6 p-5 bg-white shadow-lg rounded">
            <form id="register-form" method="post">
                <h2>Login</h2>
                <hr>
                <div class="form-group">
                    <label for="exampleInputEmail1">Correo electrónico</label>
                    <span class="text-danger">*</span><input name="email" id="email" type="email" class="form-control" placeholder="example@example.com">

                </div>
                <div class="form-group">
                    <label for="password">Contraseña</label>
                    <span class="text-danger">*</span><input name="password" id="password" type="password" class="form-control" placeholder="Password">

                </div>
                <input type="submit" class="btn btn-primary btn-block mt-5" value="Login">
            </form>
        </div>
    </div>
</div>

@endsection
