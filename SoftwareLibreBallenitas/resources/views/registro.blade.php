@extends('layout')

@section('registro')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-6 p-5 bg-white shadow-lg rounded">
            <form id="register-form" method="post">
                <h2>Registro</h2>
                <hr>
                <div class="form-group">
                    <label for="username">Usuario</label>
                    <span class="text-danger">*</span><input name="name" id="name" type="text" class="form-control" placeholder="Enter username">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Correo electrónico</label>
                    <span class="text-danger">*</span><input name="email" id="email" type="email" class="form-control" placeholder="example@example.com">
                    <small id="emailHelp" class="form-text text-muted">Nunca compartiremos su correo electrónico con nadie más</small>
                </div>
                <div class="form-group">
                    <label for="password">Contraseña</label>
                    <span class="text-danger">*</span><input name="password" id="password" type="password" class="form-control" placeholder="Password">
                    <label for="passwordConfirm">Confirmar contraseña</label>
                    <span class="text-danger">*</span><input name="confirm" id="confirm" type="password" class="form-control" placeholder="Confirm Password">
                </div>
                <input type="submit" class="btn btn-primary btn-block mt-5" value="Registrarme">
            </form>
        </div>
    </div>
</div>
@endsection
