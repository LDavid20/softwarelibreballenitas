  @extends('plantilla')


  @section('footer')

  <!-- ======= Footer ======= -->
  <footer id="footer">


    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong><span>Ballenitas S.A.</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
        Designed by <a href="#">Ballenitas S.A.</a>
      </div>
    </div>
  </footer><!-- End Footer -->

  @endsection
