@extends('layout')

@section('home')


<div class="card">
  <div class="card-header">
    You are logged in! {{Auth::user()->name}}
  </div>

  <form action="home" method="POST">
    @csrf
    <div class="input-group">
      <input type="hidden" class="form-control" placeholder="" name="id_user" value="{{Auth::user()->id}}">
      <input type="hidden" class="form-control" placeholder="" name="estado" value="espera..">

      <input type="text" class="form-control" placeholder="Link del video" name="link" aria-label="Recipient's username with two button addons" aria-describedby="button-addon4" required>
      <div class="input-group-append" id="button-addon4">
        <button class="btn btn-outline-secondary" type="submit">Button</button>

      </div>
    </div>
  </form>

  <table class="table">
    <thead>
      <tr>
        <th scope="col">Id</th>
        <th scope="col">Id_user</th>
        <th scope="col">Link</th>
        <th scope="col">Estado</th>
      </tr>
    </thead>
    <tbody>
      @foreach($descargas as $descarga)
      @if(Auth::user()->id==$descarga->id_user)
      <tr>
        <th scope="row">{{$descarga->id_descarga}}</th>
        <td>{{$descarga->id_user}}</td>
        <td>{{$descarga->link}}</td>
        <td>{{$descarga->estado}}</td>
      </tr>
      @endif
      @endforeach
    </tbody>
  </table>
</div>


@endsection